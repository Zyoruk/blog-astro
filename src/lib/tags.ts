import fs from 'node:fs';
import yaml from 'js-yaml';
import path from 'node:path';
console.log({
  url: import.meta.url,
  env: process.env
})

const url = process.env.NODE_ENV === 'production' ? new URL('meta/tags.yaml', import.meta.url) : 
  new URL(path.join(process.cwd(), '/public/meta/tags.yaml' ));
const json = fs.readFileSync(url, 'utf-8');
const tags = yaml.load(json);
console.log({tags})

export type TagContent = {
  readonly slug: string;
  readonly name: string;
};

const tagMap: { [key: string]: TagContent } = generateTagMap();

function generateTagMap(): { [key: string]: TagContent } {
  let result: { [key: string]: TagContent } = {};
  for (const tag of tags.tags) {
    result[tag.slug] = tag;
  }
  return result;
}

export function getTag(slug: string) {
  return tagMap[slug];
}

export function listTags(): TagContent[] {
  return tags.tags;
}
