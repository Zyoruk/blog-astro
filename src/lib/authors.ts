import fs from 'node:fs';
import yaml from 'js-yaml';
console.log({
  url: import.meta.url,
})
const url = new URL('meta/authors.yaml', import.meta.url);
const json = fs.readFileSync(url, 'utf-8');
const data = yaml.load(json);

export type AuthorContent = {
  readonly slug: string;
  readonly name: string;
  readonly introduction: string;
};

const authorMap: { [key: string]: AuthorContent } = generateAuthorMap();

function generateAuthorMap(): { [key: string]: AuthorContent } {
  console.log({data})
  let result: { [key: string]: AuthorContent } = {};
  for (const author of data.authors) {
    result[author.slug] = author;
  }
  return result;
}

export function getAuthor(slug: string) {
  return authorMap[slug];
}
