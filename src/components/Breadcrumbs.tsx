import { Fragment } from "react";

interface Crumb {
    uri: string;
    name: string;
}
interface Props { 
    crumbs: Crumb[];
}
export default function ({crumbs}: Props) {
    if (!crumbs) return null;

    const length = crumbs.length;
    const getBreadCrumbs = () => {
       return crumbs.map((crumb, index) => {
            if (index === length - 1){
                return <strong key={crumb.name}>{crumb.name}</strong>
            }
            return (
                <Fragment key={crumb.name}>
                    <a href={crumb.uri}>{crumb.name}</a> /    
                </Fragment>
            );
        })
    }
    return (
      <div>
        {getBreadCrumbs()}
      </div>
    )
}