import type { TagContent } from "../lib/tags";

type Props = {
  tag: TagContent;
};
export default function Tag({ tag }: Props) {
  return <a href={"/posts/tags/" + tag.slug}>{"#" + tag.name}</a>;
}
