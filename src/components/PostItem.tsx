import type { PostContent } from "../lib/posts";
import MyDate from "./Date";
import { parseISO } from "date-fns";
import Css from './PostItem.module.css';

type Props = {
  post: PostContent;
};
export default function PostItem({ post }: Props) {
  return (
    <a className={Css.a} href={"/posts/" + post.slug}>
      <MyDate date={parseISO(post.date)} />
      <h2 className={Css.h2}>{post.title}</h2>
    </a>
  );
}
