import Css from './Copyright.module.css';

export default function Copyright() {
  return <p className={Css.p}>&copy; 2020</p>
}
