import type { PostContent } from "../lib/posts";
import type { TagContent } from "../lib/tags";
import PostItem from "./PostItem";
import Pagination from "./Pagination";
import Css from './TagPostList.module.css';
import Breadcrumbs from "./Breadcrumbs";
import { Categories } from "./Categories";

type Props = {
  posts: PostContent[];
  tag: TagContent;
  tags: TagContent[];
  pagination: {
    current: number;
    pages: number;
  };
};
export default function TagPostList({ posts, tag, pagination, tags }: Props) {
  return (
    <div className={Css.container}>
      <div className={Css.posts}>
        <Breadcrumbs crumbs={[
            {
                name: "All posts",
                uri: "/posts"
            },
            {
                name: tag.name,
                uri: ""
            }
            ]
        }/>
        <ul className={Css.postList}>
          {posts.map((it, i) => (
            <li key={i}>
              <PostItem post={it} />
            </li>
          ))}
        </ul>
        <Pagination
          current={pagination.current}
          pages={pagination.pages}
          link={{
            href: (page) => (page === 1 ? '' : "/posts/page/" + page),
          }}
        />
      </div>
      <Categories tags={tags}/>
    </div>
  );
}
