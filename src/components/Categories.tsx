import type { TagContent } from "../lib/tags";
import TagLink from "./TagLink";
import Css from './Categories.module.css';

interface Props {
    tags: TagContent[]
}

export const Categories: React.FC<Props> = ({ tags }) => {
    return (
        <ul className={Css.categories}>
            {tags.map((it, i) => (
                <li className={Css.catItem} key={i}>
                    <TagLink tag={it} />
                </li>
            ))}
        </ul>

    )
}