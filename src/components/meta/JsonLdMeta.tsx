import type { BlogPosting } from "schema-dts";
import { jsonLdScriptProps } from "react-schemaorg";
import config from "../../lib/config";
import { formatISO, parseISO } from "date-fns";

type Props = {
  url: string;
  title: string;
  keywords?: string[];
  date: string;
  author?: string;
  image?: string;
  description?: string;
};
export default function JsonLdMeta({
  url,
  title,
  keywords,
  date,
  author,
  image,
  description,
}: Props) {
  console.log(parseISO(date).toDateString())
  return (
    <script
      {...jsonLdScriptProps<BlogPosting>({
        "@context": "https://schema.org",
        "@type": "BlogPosting",
        mainEntityOfPage: config.base_url + url,
        headline: title,
        keywords: keywords?.join(","),
        datePublished: parseISO(date).toString(),
        author: author,
        image: image,
        description: description,
      })}
    />
  );
}
