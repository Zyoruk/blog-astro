import { format, formatISO, parse, parseISO, toDate } from "date-fns";
import Css from './Date.module.css';

type Props = {
  date: string;
};
export default function MyDate({ date }: Props) {
  return (
    <time dateTime={format(Date.parse(date), 'LLLL d, yyyy')}>
      <span className={Css.span}>{format(Date.parse(date), 'LLLL d, yyyy')}</span>
    </time>
  );
}
