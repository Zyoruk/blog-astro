import Css from './Burger.module.css';
type Props = {
  active: boolean;
  onClick: () => void;
};
export default function Burger({ active, onClick }: Props) {
  return (
    <div className={`${Css.container} ${active ? Css.active : ""}`} onClick={onClick}>
      <div className={`${Css.meat} ${Css.meat1}`} />
      <div className={`${Css.meat} ${Css.meat2}`} />
      <div className={`${Css.meat} ${Css.meat3}`} />
    </div>
  );
}
