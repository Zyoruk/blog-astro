import type { TagContent } from "../lib/tags";
import Css from './TagButton.module.css';

type Props = {
  tag: TagContent;
};
export default function TagButton({ tag }: Props) {
  return <a className={Css.a} href={"/posts/tags/[[...slug]]"}>{tag.name}</a>;
}
