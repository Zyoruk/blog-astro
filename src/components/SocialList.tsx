import config from "../lib/config";
import Css from './SocialList.module.css';

export function SocialList({}) {
  return (
    <div>
        <a className={Css.a}
            title="Bio Devpand"
            href={`https://bio.devpand.com`}
            target="_blank"
            rel="noreferrer"
        >
        <figure className={Css.navigationIconContainer}>
            <img src="/favicon.svg" alt="zyoruk icon" className={Css.navigationIcon} width={24} height={24}/>
        </figure>
      </a>
      <a className={Css.a}
        title="GitHub"
        href={`https://github.com/${config.github_account}`}
        target="_blank"
        rel="noreferrer"
      >
        <img src="/github-alt.svg" width={24} height={24}  />
      </a>
      <a className={Css.a}
        title="Gitlab"
        href={`https://gitlab.com/${config.gitlab_account}`}
        target="_blank"
        rel="noreferrer"
      >
        <img src="/gitlab-alt.svg" width={24} height={24}  />
      </a>
    </div>
  );
}
