import { generatePagination } from "../lib/pagination";
import Css from './Pagination.module.css';

type Props = {
  current: number;
  pages: number;
  link: {
    href: (page: number) => string;
  };
};
export default function Pagination({ current, pages, link }: Props) {
  const pagination = generatePagination(current, pages);
  return (
    <ul className={Css.ul}>
      {pagination.map((it, i) => (
        <li className={Css.li} key={i}>
          {it.excerpt ? (
            "..."
          ) : (
            <a
              className={`${Css.a} ${it.page === current ? Css.active : undefined}` }
              href={link.href(Number(it.page))}>
              {Number(it.page)}
            </a>
          )}
        </li>
      ))}
    </ul>
  );
}
