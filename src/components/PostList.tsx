import type { PostContent } from "../lib/posts";
import PostItem from "./PostItem";
import Pagination from "./Pagination";
import type { TagContent } from "../lib/tags";
import Css from './PostList.module.css';
import { Categories } from "./Categories";
import Breadcrumbs from "./Breadcrumbs";

type Props = {
  posts: PostContent[];
  tags: TagContent[];
  pagination: {
    current: number;
    pages: number;
  };
};
export default function PostList({ posts, tags, pagination }: Props) {
  return (
    <div className={Css.container}>
      <div className={Css.posts}>
        <Breadcrumbs crumbs={[
                  {
                      name: "All posts",
                      uri: "/posts"
                  },
              ]
          }/>
        <ul className={Css.postList}>
          {posts.map((it, i) => (
            <li className={Css.postItem} key={i}>
              <PostItem post={it} />
            </li>
          ))}
        </ul>
        <Pagination
          current={pagination.current}
          pages={pagination.pages}
          link={{
            href: (page) => (page === 1 ? '' : "/posts/page/" + page)
          }}
        />
      </div>
      <Categories tags={tags} />
    </div>
  );
}
