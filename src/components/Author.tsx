import type { AuthorContent } from "../lib/authors";
import Css from './Author.module.css';
type Props = {
  author: AuthorContent;
};
export default function Author({ author }: Props) {
  return <span className={Css.span}>{author.name}</span>
}
